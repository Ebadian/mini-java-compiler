package symboltable;

public class ClassTable extends Table {
    public ClassTable(String name) {
        super(name);
    }

    public MethodRow addMethod(String name) {
        MethodRow newRow = new MethodRow(name);
        rows.add(newRow);
        return newRow;
    }
}
