package symboltable;

import symboltable.exceptions.AlreadyExistsException;
import symboltable.exceptions.WrongCommandException;

import java.util.ArrayList;

public abstract class Table {
    protected ArrayList<Row> rows;
    protected String name;
    protected Table parent;

    public Table(String name) {
        this.name = name;
        rows = new ArrayList<>();
    }

    public Table(String name, Table parent) {
        this(name);
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public void setParent(Table parent) {
        if (this.parent != null)
            throw new WrongCommandException(this, "SetParent");
        this.parent = parent;
    }

    public Table getParent() {
        return parent;
    }

    public VariableRow lookupVariableInTable(String name) {
        for (Row r : rows)
            if (r.getRowType().equals(RowType.Variable) && r.getName().equals(name))
                return (VariableRow) r;
        return null;
    }

    public VariableRow lookupVariable(String name) {
        VariableRow r = lookupVariableInTable(name);
        if (r == null && parent != null)
            r = parent.lookupVariable(name);
        return r;
    }

    public MethodRow lookupMethodInTable(String name, ArrayList<VariableType> signature, boolean checkSignature) {
        for (Row r : rows)
            if (r.getRowType().equals(RowType.Method)
                    && r.getName().equals(name) && (!checkSignature || ((MethodRow) r).signature.equals(signature)))
                return (MethodRow) r;
        return null;
    }

    public MethodRow lookupMethodInTable(String name, ArrayList<VariableType> signature) {
        return lookupMethodInTable(name, signature, true);
    }

    public MethodRow lookupMethod(String name, ArrayList<VariableType> signature, boolean checkSignature) {
        MethodRow m = lookupMethodInTable(name, signature, checkSignature);
        if (m == null && parent != null)
            m = parent.lookupMethod(name, signature, checkSignature);
        return m;
    }

    public MethodRow lookupMethod(String name, ArrayList<VariableType> signature) {
        return lookupMethod(name, signature, true);
    }

    public Row popRow() {
        return rows.remove(rows.size() - 1);
    }

    public VariableRow addVariable(String name) throws AlreadyExistsException {
        if (lookupVariableInTable(name) != null)
            throw new AlreadyExistsException(name, RowType.Variable.toString());

        VariableRow newRow = new VariableRow(name);
        rows.add(newRow);
        return newRow;
    }

    @Override
    public String toString() {
        String s = super.toString() + "# Rows:";
        for (Row r : rows)
            s += "\n    # " + r.toString() + " #";
        s += "\n";
        return s;
    }
}

