package symboltable;

public class Row {
    private String name;
    private RowType rowType;
    private int address;

    public Row(String name, RowType rowType) {
        this.name = name;
        this.rowType = rowType;
    }

    public String getName() {
        return name;
    }

    public RowType getRowType() {
        return rowType;
    }

    public void setRowType(RowType rowType) {
        this.rowType = rowType;
    }

    public int getAddress() {
        return address;
    }

    public void setAddress(int address) {
        this.address = address;
    }

    @Override
    public String toString() {
        String s = super.toString() + " Name:" + name + " Type:" + rowType.toString() + " address:" + address;
        return s;
    }
}
