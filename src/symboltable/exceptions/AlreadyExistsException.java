package symboltable.exceptions;

public class AlreadyExistsException extends Exception {
    public AlreadyExistsException(String name, String type) {
        super("Already exists " + type + " with name " + name);
    }
}
