package symboltable.exceptions;

/**
 * Created by parand on 1/26/18.
 */
public class WrongObjectInStackException extends RuntimeException{
    public WrongObjectInStackException(Object o) {
        super(o.toString() + " is misplaced in semantic stack");
    }
}
