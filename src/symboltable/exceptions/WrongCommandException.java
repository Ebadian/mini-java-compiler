package symboltable.exceptions;

import symboltable.Table;

public class WrongCommandException extends RuntimeException {
    public WrongCommandException(Table table, String command) {
        super("Wrong command " + command + " at table " + (table == null ? "null" : table.getName()));
    }
}
