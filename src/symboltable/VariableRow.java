package symboltable;

public class VariableRow extends Row {
    private VariableType variableType;

    VariableRow(String name) {
        super(name, RowType.Variable);
        this.variableType = variableType;
    }

    public VariableType getVariableType() {
        return variableType;
    }

    public void setVariableType(VariableType variableType) {
        this.variableType = variableType;
    }
}
