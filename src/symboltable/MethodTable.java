package symboltable;

public class MethodTable extends Table {
    private MethodRow relativeRow;
    private int returnAddress;

    private VariableType returnType;

    public MethodTable(String name, ClassTable parentClass, MethodRow relativeRow) {
        super(name, parentClass);
        this.relativeRow = relativeRow;
    }

    public MethodRow addParameter(VariableType type, int address) {
        relativeRow.addToSignature(type, address);
        return relativeRow;
    }

    public VariableType getReturnType() {
        return returnType;
    }

    public int getReturnAddress() {
        return returnAddress;
    }

    public void setReturn(VariableType returnType, int address) {
        this.returnType = returnType;
        this.returnAddress = address;
        relativeRow.setReturn(returnType, returnAddress);
    }

    public MethodRow getRelativeRow() {
        return relativeRow;
    }
}
