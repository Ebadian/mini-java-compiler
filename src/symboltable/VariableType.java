package symboltable;

public enum VariableType {
    Integer("int"), Boolean("boolean");

    String typeString;
    VariableType(String type) {
        this.typeString = type;
    }

    public static VariableType get(String typeString) {
        for (VariableType type : VariableType.values())
            if (type.typeString.equals(typeString))
                return type;
        return null;
    }
}
