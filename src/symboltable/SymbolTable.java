package symboltable;

import symboltable.exceptions.AlreadyExistsException;
import symboltable.exceptions.WrongCommandException;

import java.util.ArrayList;

/**
 * Created by parand on 1/25/18.
 */
public class SymbolTable {
    ArrayList<Table> tables;
    private static SymbolTable symbolTable;
    public Table currentTable;

    private SymbolTable() {
        tables = new ArrayList<>();
    }

    public static SymbolTable getInstance() {
        if (symbolTable == null)
            symbolTable = new SymbolTable();
        return symbolTable;
    }

    public ClassTable addClass(String name) throws AlreadyExistsException {
        if (currentTable != null) {
            throw new WrongCommandException(currentTable, "AddClass");
        }

        for (Table t : tables)
            if (name.equals(t.getName()))
                throw new AlreadyExistsException(name, "Class");

        ClassTable t = new ClassTable(name);
        tables.add(t);
        currentTable = t;
        //System.out.println("opened class " + currentTable.getName());
        return t;
    }

    public boolean closeClassTable() {
        if (currentTable == null)
            return false;
        if (!(currentTable instanceof ClassTable))
            throw new WrongCommandException(currentTable, "CloseClass");
        //System.out.println("closed class " + currentTable.getName());
        currentTable = null;
        return true;
    }

    public ClassTable lookupClass(String name) {
        for (Table t : tables)
            if (name.equals(t.getName()))
                return (ClassTable) t;
        return null;
    }

    public VariableRow addField(String name) throws AlreadyExistsException {
        if (currentTable == null || !(currentTable instanceof ClassTable))
            throw new WrongCommandException(currentTable, "AddField");

        return currentTable.addVariable(name);
    }

    public MethodRow addParameter(VariableType type, int address) {
        if (currentTable == null || !(currentTable instanceof MethodTable))
            throw new WrongCommandException(currentTable, "AddParameter");

        MethodTable methodTable = (MethodTable) currentTable;
        return methodTable.addParameter(type, address);
    }

    public MethodTable initMethod(String name) {
        if (currentTable == null || !(currentTable instanceof ClassTable))
            throw new WrongCommandException(currentTable, "AddMethod");

        ClassTable c = (ClassTable) currentTable;

        MethodRow methodRow = c.addMethod(name);
        MethodTable methodTable = new MethodTable(name, c, methodRow);
        currentTable = methodTable;
        //System.out.println("opened method " + name);
        return methodTable;
    }

    public boolean closeMethodTable() {
        if (currentTable == null)
            return false;
        if (!(currentTable instanceof MethodTable))
            throw new WrongCommandException(currentTable, "closeMethod");
        //System.out.println("closed method " + currentTable.getName());
        currentTable = currentTable.parent;
        return true;
    }

    public VariableRow addVariable(String name) throws AlreadyExistsException {
        if (currentTable == null || currentTable instanceof ClassTable)
            throw new WrongCommandException(currentTable, "AddVariable");

        return currentTable.addVariable(name);
    }
}

