package symboltable;

import java.util.ArrayList;

public class MethodRow extends Row {
    ArrayList<VariableType> signature;
    ArrayList<Integer> addresses;

    private VariableType returnType;
    private int returnAddress;

    private int startingLine;

    public MethodRow(String name) {
        super(name, RowType.Method);
        signature = new ArrayList<>();
        addresses = new ArrayList<>();
    }

    public int getStartingLine() {
        return startingLine;
    }

    public void setStartingLine(int startingLine) {
        this.startingLine = startingLine;
    }

    public void addToSignature(VariableType type, int address) {
        signature.add(type);
        addresses.add(address);
    }

    public ArrayList<Integer> getAddresses() {
        return addresses;
    }

    public void setReturn(VariableType returnType, int returnAddress) {
        this.returnAddress = returnAddress;
        this.returnType = returnType;
    }

    public VariableType getReturnType() {
        return returnType;
    }

    public int getReturnAddress() {
        return returnAddress;
    }

    public ArrayList<VariableType> getSignature() {
        return signature;
    }

    @Override
    public String toString() {
        String s = super.toString();
        s += "  signature:" + signature.toString();
        return s;
    }
}
