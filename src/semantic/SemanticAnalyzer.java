package semantic;

import errorHandler.ErrorHandler;
import scanner.Token;
import symboltable.*;
import symboltable.exceptions.AlreadyExistsException;

public class SemanticAnalyzer {
    private SymbolTable symbolTable;

    public SemanticAnalyzer(SymbolTable symbol) {
        this.symbolTable = symbol;
    }

    public void analyze(String command, Token token) {
        String lexeme = null;
        if (token != null)
            lexeme = token.getLexeme();

        switch (command) {
            case "#decClass":
                try {
                    symbolTable.addClass(lexeme);
                } catch (AlreadyExistsException e) {
                    ErrorHandler.Error("Semantic Analyzer" , e.getMessage());
                    symbolTable.currentTable = symbolTable.lookupClass(lexeme);
                    //System.out.println("reopened class " + lexeme);
                }
                break;

            case "#closeClass":
                symbolTable.closeClassTable();
                break;

            case "#addExtension": {
                Table parent = symbolTable.lookupClass(lexeme);
                if (parent != null) {
                    if (parent.equals(symbolTable.currentTable)) {
                        ErrorHandler.Error("Semantic Analyzer","Wrong extension. Cannot extend himself.");
                    } else {
                        symbolTable.currentTable.setParent(parent);
                    }
                } else {
                    ErrorHandler.Error("Semantic Analyzer","Wrong extension. No class found with name " + lexeme);
                }
            }
            break;

            case "#decField":
                try {
                    symbolTable.addField(lexeme);
                } catch (AlreadyExistsException e) {
                    ErrorHandler.Error("Semantic Analyzer","(Field Declaration) " + e.getMessage());
                }
                break;

            case "#addVar":
                try {
                    symbolTable.addVariable(lexeme);
                } catch (AlreadyExistsException e) {
                    ErrorHandler.Error("Semantic Analyzer","(Variable Declaration) " + e.getMessage());
                }
                break;

            case "#decMethod":
            case "#decMainMethod":
                symbolTable.initMethod(lexeme);
                break;

            case "#addParam": {
                try {
                    symbolTable.addVariable(lexeme);
                } catch (AlreadyExistsException e) {
                    ErrorHandler.Error("Semantic Analyzer", "(Adding Parameter)" + e.getMessage());
                }
            }
            break;

            case "#finMethodDec":
                MethodTable currentMethod = (MethodTable) symbolTable.currentTable;
                MethodRow relativeRow = currentMethod.getRelativeRow();
                Table parent = currentMethod.getParent();
                //System.out.println("@ finMethodDec " + parent);
                MethodRow r = parent.lookupMethodInTable(currentMethod.getName(),
                        relativeRow.getSignature());
                if (r != relativeRow) {
                    parent.popRow();
                    ErrorHandler.Error("Semantic Analyzer",
                            "(Finishing Declaring Method) Method \""
                                    + relativeRow.getName() + "\" signature:" + relativeRow.getSignature()
                                    + " has been declared before.");

                }
                break;

            case "#finishMethod":
                symbolTable.closeMethodTable();
                break;

            default:
        }
    }
}
