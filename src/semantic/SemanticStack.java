package semantic;

import java.util.ArrayList;

public class SemanticStack<T> {
    private ArrayList<T> stack;

    public SemanticStack() {
        stack = new ArrayList<>();
    }

    public void push(T o) {
        stack.add(o);
    }

    public void pop(int count) {
        for (int i = 0; i < count; i++)
            stack.remove(stack.size() - 1);
    }

    public T pop() {
        return stack.remove(stack.size() - 1);
    }

    public T get(int index) {
        return stack.get(stack.size() - 1 - index);
    }

    public T top() {
        return get(0);
    }

    public void print() {
        for (int i = 0 ; i < stack.size() ; i ++)
            System.out.print(stack.get(i).toString() + " ## ");
        System.out.println();
    }
}
