package semantic;

import symboltable.VariableType;

/**
 * Created by parand on 1/27/18.
 */
public class SSEntry {
    private String address;
    private VariableType varType = VariableType.Integer;


    public void setConstant(int x){
        address = "#" + x;
        varType = VariableType.Integer;
    }
    public void setConstant(boolean x){
        address = "#" + (x ? 1 : 0);
        varType = VariableType.Boolean;
    }

    public SSEntry() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public VariableType getVarType() {
        return varType;
    }

    public void setVarType(VariableType varType) {
        this.varType = varType;
    }

    public SSEntry(String address, VariableType varType) {

        this.address = address;
        this.varType = varType;
    }

    public SSEntry(String address) {

        this.address = address;
    }

    @Override
    public String toString() {
        return "SSEntry: adrs: " + address + " " + varType.toString();
    }
}
