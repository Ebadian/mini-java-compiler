package generator;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Created by parand on 1/26/18.
 */
public class Quadruples {
    private ArrayList<String> codes = new ArrayList<>();


    public ArrayList<String> getCodes() {
        return codes;
    }

    public int getSize(){
        return codes.size();
    }

    public boolean setLine(int i, String s){
        if(i >= getSize())
            return false;
        codes.set(i , s);
        return true;
    }
    public void addLine(String s){
        codes.add(s);
    }

    public String JPF(String check , String address){
        return "(JPF, " + check + ", " + address + ", )";
    }
    public String JP(String address){
        return "(JP, " + address + ", , )";
    }

    public String Assign(String value, String address){
        return "(ASSIGN, " + value + ", " + address + ", )";
    }

    public String Add(String S1, String S2, String D){
        return "(ADD, " + S1 + ", " + S2 + ", " + D + ")";
    }
    public String Sub(String S1, String S2, String D){
        return "(SUB, " + S1 + ", " + S2 + ", " + D + ")";
    }

    public String And(String S1, String S2, String D){
        return "(AND, " + S1 + ", " + S2 + ", " + D + ")";
    }
    public String Print(String S){
        return "(PRINT, " + S + ", , )";
    }

    public String Eq(String S1, String S2, String D){
        return "(EQ, " + S1 + ", " + S2 + ", " + D + ")";
    }

    public String Lt(String S1, String S2, String D){
        return "(LT, " + S1 + ", " + S2 + ", " + D + ")";
    }


    public String Mult(String S1, String S2, String D){
        return "(MULT, " + S1 + ", " + S2 + ", " + D + ")";
    }

    public void print(){
        try {
        PrintWriter pw = new PrintWriter(new FileWriter("./output.txt"));
        for (int i = 0 ; i < codes.size(); i ++)
            pw.println(i + "\t" + codes.get(i));
        pw.close();
        }
        catch (Exception e){
            System.out.println("No such file as output.txt");
        }
    }
}
