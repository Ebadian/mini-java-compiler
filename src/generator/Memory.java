package generator;

import java.util.ArrayList;

/**
 * Created by parand on 1/25/18.
 */
public class Memory {
    public static final int SP = 1000;
    public static final int FP = 1001;
    public static final int RV = 1002;

    private static final int start = 1003;
    private static final int end = 5000;
    private static int pointer = start;


    private static final int startTemp = 5001;
    private static final int tempRange = 1000;
    private static int tempPointer = 0;
    private static Quadruples quadruples;

    public static void setQuadruples(Quadruples quadruples) {
        Memory.quadruples = quadruples;
    }

    public static int getNew(){
        return pointer ++;
    }

    public static int getTemp(){
        tempPointer ++;
        int x = ((tempPointer - 1 )% tempRange) + startTemp;
        quadruples.addLine(quadruples.Assign("#0", Integer.toString(x)));
        return x;

    }
}
