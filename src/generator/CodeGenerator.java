package generator;

import errorHandler.ErrorHandler;
import scanner.Token;
import semantic.SSEntry;
import semantic.SemanticStack;
import symboltable.*;
import symboltable.exceptions.WrongObjectInStackException;

import java.util.ArrayList;
import java.util.Collections;

public class CodeGenerator {
    private SemanticStack ss;
    private SymbolTable symbolTable;
    private Quadruples quadruples;


    public CodeGenerator(SemanticStack ss, SymbolTable symbol, Quadruples q) {
        this.ss = ss;
        this.symbolTable = symbol;
        this.quadruples = q;

    }

    public void generate(String command, Token token) {
       // System.out.println("At generate command = " + command);
        String lexeme = token == null ? null : token.getLexeme();

        //ss.print();
        switch (command) {
            case "#pid": {
                VariableRow v = symbolTable.currentTable.lookupVariable(lexeme);
                SSEntry s = new SSEntry("433332");
                if (v == null) {
                    ErrorHandler.IdNotFound(lexeme);
                } else {
                    s.setAddress(Integer.toString(v.getAddress()));
                    s.setVarType(v.getVariableType());
                }
                ss.push(s);
            }
            break;

            case "#pushTrue": {
                SSEntry s = new SSEntry();
                s.setConstant(true);
                ss.push(s);
            }
            break;


            case "#pushFalse": {
                SSEntry s = new SSEntry();
                s.setConstant(false);
                ss.push(s);
            }
            break;

            case "#label":
                ss.push(quadruples.getSize());
                break;

            case "#save":
                ss.push(quadruples.getSize());
                quadruples.addLine("");
                break;

            case "#jpToMain": {
                Object o = ss.top();

                if (!(o instanceof Integer))
                    throw new WrongObjectInStackException(o);
                int x = (Integer) o;

                quadruples.setLine(x, quadruples.JP(Integer.toString(quadruples.getSize())));


                break;
            }

            case "#decClass":
                break;

            case "#closeClass":
                break;

            case "#addExtension":
                break;

            case "#saveType":
                ss.push(VariableType.get(lexeme));
                break;

            case "#addVar":
            case "#decField": {
                VariableType type = (VariableType) ss.pop();
                VariableRow row = symbolTable.currentTable.lookupVariableInTable(lexeme);
                row.setVariableType(type);
                int address = Memory.getNew();
                quadruples.addLine(quadruples.Assign("#0", Integer.toString(address)));
                row.setAddress(address);
            }
            break;

            case "#addParam":
            {
                VariableType type = (VariableType) ss.pop();
                VariableRow row = symbolTable.currentTable.lookupVariableInTable(lexeme);
                row.setVariableType(type);
                int address = Memory.getNew();
                quadruples.addLine(quadruples.Assign("#0", Integer.toString(address)));
                row.setAddress(address);
                symbolTable.addParameter(type, address);
            }
            break;

            case "#decMethod": {
                VariableType type = (VariableType) ss.pop();
                MethodTable currentMethod = ((MethodTable) symbolTable.currentTable);
                int address = Memory.getNew();
                currentMethod.setReturn(type, address);

                quadruples.addLine(
                        quadruples.Assign("#0", Integer.toString(address))
                );
            }
            case "#decMainMethod":
                break;

            case "#finishMethod":
                break;

            case "#finMethodDec":
                MethodTable currentMethod = ((MethodTable) symbolTable.currentTable);
                currentMethod.getRelativeRow().setStartingLine(quadruples.getSize());
                break;

            case "#jpfSave":
                Object o = ss.top();
                if (!(o instanceof Integer))
                    throw new WrongObjectInStackException(o);
                int x = (Integer) o;
                o = ss.get(1);
                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                SSEntry y = (SSEntry) o;

                quadruples.setLine(x, quadruples.JPF(y.getAddress(), Integer.toString(quadruples.getSize() + 1)));

                ss.pop(2);
                ss.push(quadruples.getSize());
                quadruples.addLine("");

                break;

            case "#jp":
                o = ss.pop();
                if (!(o instanceof Integer))
                    throw new WrongObjectInStackException(o);
                x = (Integer) o;
                quadruples.setLine(x, quadruples.JP(Integer.toString(quadruples.getSize())));
                break;

            case "#while":
                ss.print();
                o = ss.top();

                if (!(o instanceof Integer))
                    throw new WrongObjectInStackException(o);
                x = (Integer) o;

                o = ss.get(1);
                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                y = (SSEntry) o;

                o = ss.get(2);

                if (!(o instanceof Integer))
                    throw new WrongObjectInStackException(o);
                int z = (Integer) o;

                quadruples.setLine(x, quadruples.JPF(y.getAddress(), Integer.toString(quadruples.getSize() + 1)));
                quadruples.addLine(quadruples.JP(Integer.toString(z)));
                ss.pop(3);
                break;

            case "#pushInt":
                SSEntry ssEntry = new SSEntry();
                ssEntry.setConstant(Integer.parseInt(token.getLexeme()));
                ss.push(ssEntry);
                break;

            case "#assignTmp":
                o = ss.top();

                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);

                y = (SSEntry) o;

                ss.pop(1);

                x = getTemp();
                quadruples.addLine(quadruples.Assign(y.getAddress(), Integer.toString(x)));
                ssEntry = new SSEntry(Integer.toString(x), y.getVarType());

                ss.push(ssEntry);
                break;

            case "#assign":
                o = ss.top();

                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                SSEntry val = (SSEntry) o;

                o = ss.get(1);
                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                SSEntry adr = (SSEntry) o;
                ss.pop(2);
                if(!val.getVarType().equals(adr.getVarType())){
                    ErrorHandler.TypesDontMatch();
                    break;
                }
                quadruples.addLine(quadruples.Assign(val.getAddress(), adr.getAddress()));

                break;


            case "#for":
                o = ss.top();

                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                val = (SSEntry) o;

                o = ss.get(1);
                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                adr = (SSEntry) o;


                quadruples.addLine(quadruples.Add(adr.getAddress(), val.getAddress(), adr.getAddress()));
                ss.pop(2);

                o = ss.top();

                if (!(o instanceof Integer))
                    throw new WrongObjectInStackException(o);
                x = (Integer) o;

                o = ss.get(1);

                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                y = (SSEntry) o;

                o = ss.get(2);

                if (!(o instanceof Integer))
                    throw new WrongObjectInStackException(o);
                z = (Integer) o;


                quadruples.addLine(quadruples.JP(Integer.toString(z)));

                //quadruples.addLine(quadruples.JP(Integer.toString(x - 1)));
                quadruples.setLine(x, quadruples.JPF(y.getAddress(), Integer.toString(quadruples.getSize())));
                ss.pop(3);


                break;
            case "#print":

                o = ss.pop();

                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                y = (SSEntry) o;

                quadruples.addLine(quadruples.Print(y.getAddress()));
                break;

            case "#sum":
                o = ss.top();

                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                val = (SSEntry) o;

                o = ss.get(1);


                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                adr = (SSEntry) o;


                x = getTemp();
                ss.pop(2);
                y = new SSEntry(Integer.toString(x));
                y.setVarType(val.getVarType());

                ss.push(y);

                if(val.getVarType().equals(VariableType.Boolean)) {
                    ErrorHandler.IntegerExpected();
                    break;
                }


                quadruples.addLine(quadruples.Add(val.getAddress(), adr.getAddress(), Integer.toString(x)));
                break;

            case "#minus":
                o = ss.top();

                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                val = (SSEntry) o;

                o = ss.get(1);


                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                adr = (SSEntry) o;


                x = getTemp();
                ss.pop(2);
                y = new SSEntry(Integer.toString(x));
                y.setVarType(val.getVarType());

                ss.push(y);


                if (val.getVarType().equals(VariableType.Boolean)) {
                    ErrorHandler.IntegerExpected();
                    break;
                }


                quadruples.addLine(quadruples.Sub(val.getAddress(), adr.getAddress(), Integer.toString(x)));
                break;

            case "#mult":
                o = ss.top();

                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                val = (SSEntry) o;

                o = ss.get(1);


                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                adr = (SSEntry) o;


                x = getTemp();
                ss.pop(2);
                y = new SSEntry(Integer.toString(x));
                y.setVarType(val.getVarType());

                ss.push(y);

                if (val.getVarType().equals(VariableType.Boolean)) {
                    ErrorHandler.IntegerExpected();
                    break;
                }


                quadruples.addLine(quadruples.Mult(val.getAddress(), adr.getAddress(), Integer.toString(x)));
                break;


            case "#and":

                o = ss.top();

                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                val = (SSEntry) o;

                o = ss.get(1);


                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                adr = (SSEntry) o;


                x = getTemp();
                ss.pop(2);
                y = new SSEntry(Integer.toString(x));
                y.setVarType(val.getVarType());

                ss.push(y);


                if (val.getVarType().equals(VariableType.Integer)) {
                    ErrorHandler.BoolExpected();
                    break;
                }


                quadruples.addLine(quadruples.And(val.getAddress(), adr.getAddress(), Integer.toString(x)));
                break;

            case "#eq":
                o = ss.top();

                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                val = (SSEntry) o;

                o = ss.get(1);


                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                adr = (SSEntry) o;


                x = getTemp();
                ss.pop(2);
                y = new SSEntry(Integer.toString(x));
                y.setVarType(val.getVarType());

                ss.push(y);

                if (!val.getVarType().equals(adr.getVarType())) {
                    ErrorHandler.TypesDontMatch();
                    break;
                }


                quadruples.addLine(quadruples.Eq(val.getAddress(), adr.getAddress(), Integer.toString(x)));
                break;

            case "#lt":

                o = ss.top();

                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                val = (SSEntry) o;

                o = ss.get(1);


                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                adr = (SSEntry) o;


                x = getTemp();
                ss.pop(2);
                y = new SSEntry(Integer.toString(x));
                y.setVarType(val.getVarType());

                ss.push(y);

                if (!val.getVarType().equals(adr.getVarType())) {
                    ErrorHandler.TypesDontMatch();
                    break;
                }

                if (val.getVarType().equals(VariableType.Boolean)) {
                    ErrorHandler.IntegerExpected();
                    break;
                }


                quadruples.addLine(quadruples.Lt(adr.getAddress(), val.getAddress(), Integer.toString(x)));
                break;

            case "#return": {
                SSEntry s = (SSEntry) ss.pop();
                MethodTable m = (MethodTable) symbolTable.currentTable;
                if (!s.getVarType().equals(m.getReturnType())) {
                    ErrorHandler.Error("Semantic Analyzer" , "Expected return type "
                            + m.getReturnType() + " but found " + s.getVarType());
                }
                quadruples.addLine(
                        quadruples.Assign(s.getAddress(), Integer.toString(Memory.RV))
                );
                quadruples.addLine(
                        quadruples.JP("@" + Integer.toString(m.getReturnAddress()))
                );
            }
            break;

            case "#saveClass": {
                ClassTable classTable = symbolTable.lookupClass(lexeme);
                if (classTable == null)
                    ErrorHandler.Error("Semantic Analyzer", "No class found with name " + lexeme);
                ss.push(classTable);
            }
            break;

            case "#findVar": {
                ClassTable classTable = (ClassTable) ss.pop();
                SSEntry s = new SSEntry("433332");
                if (classTable != null) {
                    VariableRow r = classTable.lookupVariable(lexeme);
                    if (r != null) {
                        s.setAddress(Integer.toString(r.getAddress()));
                        s.setVarType(r.getVariableType());
                    }
                }
                ss.push(s);
            }
            break;

            case "#addMethodName":
                ss.push(lexeme);
                ss.push(0);
                break;

            case "#addArg": {
                SSEntry s = (SSEntry) ss.pop();
                int cnt = (int) ss.pop();
                ss.push(s);
                ss.push(cnt + 1);
            }
            break;

            case "#callMethod": {
                int argc = (int) ss.pop();
                ArrayList<SSEntry> arguments = new ArrayList<>();
                ArrayList<VariableType> signature = new ArrayList<>();
                for (int i = 0; i < argc; i++) {
                    SSEntry s = (SSEntry) ss.pop();
                    arguments.add(s);
                    signature.add(s.getVarType());
                }

                Collections.reverse(arguments);
                Collections.reverse(signature);

                String methodName = (String) ss.pop();
                ClassTable classTable = (ClassTable) ss.pop();

                VariableType type = VariableType.Integer;

                if (classTable != null) {
                    MethodRow method = classTable.lookupMethod(methodName, signature);
                    if (method != null) {
                        ArrayList<Integer> addresses = method.getAddresses();
                        for (int i = 0; i < argc; i++) {
                            quadruples.addLine(
                                    quadruples.Assign(
                                            arguments.get(i).getAddress(), Integer.toString(addresses.get(i))));
                        }

                        quadruples.addLine(
                                quadruples.Assign(
                                        "#" + Integer.toString(quadruples.getSize() + 2),
                                         Integer.toString(method.getReturnAddress()))
                        );

                        quadruples.addLine(
                                quadruples.JP(
                                        Integer.toString(method.getStartingLine())
                                )
                        );

                        type = method.getReturnType();
                    } else {
                        MethodRow possibleMethod = classTable.lookupMethod(methodName, signature, false);
                        if (possibleMethod == null) {
                            ErrorHandler.Error(
                                    "Semantic Analyzer", "No method found with name "
                                            + methodName + " in class " + classTable.getName());
                        } else {
                            int argcPM = possibleMethod.getSignature().size();
                            if (argc < argcPM) {
                                ErrorHandler.Error(
                                        "Semantic Analyzer",  "Too few arguments for function "
                                                + methodName + " in class " + classTable.getName());
                            } else if (argc > argcPM) {
                                ErrorHandler.Error(
                                        "Semantic Analyzer",  "Too many arguments for function "
                                                + methodName + " in class " + classTable.getName());
                            } else {
                                ErrorHandler.Error(
                                        "Semantic Analyzer",  "Wrong argument types for function "
                                                + methodName + " in class " + classTable.getName());
                            }
                        }
                    }
                }

                int returnValueAddress = getTemp();
                quadruples.addLine(
                        quadruples.Assign(Integer.toString(Memory.RV), Integer.toString(returnValueAddress))
                );
                ss.push(new SSEntry(Integer.toString(returnValueAddress), type));
            }
            break;

            case "#initial":
                quadruples.addLine(quadruples.Assign("#0", Integer.toString(Memory.FP)));
                quadruples.addLine(quadruples.Assign("#0", Integer.toString(Memory.SP)));
                quadruples.addLine(quadruples.Assign("#0", Integer.toString(Memory.RV)));
                break;

            case "#checkType":

                o = ss.top();

                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                val = (SSEntry) o;

                o = ss.get(1);
                if (!(o instanceof SSEntry))
                    throw new WrongObjectInStackException(o);
                adr = (SSEntry) o;

                if(!val.getVarType().equals(adr.getVarType()))
                    ErrorHandler.TypesDontMatch();
                break;
                
            default:
                System.err.println("Command not found " + command);
        }
    }

    private int getTemp() {
        int address = Memory.getTemp();
//        quadruples.addLine(
//                quadruples.Assign("#0", Integer.toString(address))
//        );
        return address;
    }
}
