import parser.AllRules;
import parser.Parser;
import scanner.LexicalAnalyzer;
import scanner.Token;

public class Main {
    public static void main(String[] args) {
        //AllRules allRules = new AllRules();
        //allRules.print();

        if (args.length < 1) {
            System.err.println("No files passed to compile.");
            return;
        }

        String fileAddress = args[0];
//        LexicalAnalyzer scanner = new LexicalAnalyzer(fileAddress);
//
//        Token token;
//        while ((token = scanner.getNextToken()) != null) {
//            System.out.println("TOKEN " + token.getTokenType() + " lexeme: " + token.getLexeme());
//        }
        Parser parser = new Parser(fileAddress);
        parser.parse();
    }
}
