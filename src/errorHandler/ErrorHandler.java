package errorHandler;

import scanner.LexicalAnalyzer;

/**
 * Created by parand on 1/27/18.
 */
public class ErrorHandler {
    private static LexicalAnalyzer scanner;

    public static void setScanner(LexicalAnalyzer scanner) {
        ErrorHandler.scanner = scanner;
    }

    public static void Error(String errorFinder, String errorMsg){
        System.out.println((scanner.getLineCounter() + 1) + ":" + (scanner.getLinePointer() + 1)
                + " [" + errorFinder + "]" + " Error: " + errorMsg);
    }
    public static void TypesDontMatch(){
        Error("Semantic Analyzer","types don't match" );
    }
    public static void IdNotFound(String lexeme){

        Error("Semantic Analyzer","Identifier not found named " + lexeme );

    }

    public static void BoolExpected(){


        Error("Semantic Analyzer","boolean expression expected but integer found" );
    }
    public static void IntegerExpected(){


        Error("Semantic Analyzer","integer expression expected but  boolean found" );
    }

}
