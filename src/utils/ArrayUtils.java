package utils;

import java.util.ArrayList;

/**
 * Created by parand on 1/19/18.
 */
public class ArrayUtils {

    public static <E> ArrayList<E> unique(ArrayList<E> a){
        ArrayList<E> res = new ArrayList<>();
        for (int i = 0 ; i < a.size(); i ++){
            E t = a.get(i);
            boolean tf = true;
            for (int j = 0 ; j < res.size(); j ++)
                if(res.get(j).equals(t))
                    tf = false;
            if(tf)
                res.add(t);

        }
        return res;
    }


}
