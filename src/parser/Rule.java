package parser;

import java.util.ArrayList;

/**
 * Created by parand on 1/19/18.
 */
public class Rule {
    private String LHS;
    private ArrayList<String> RHS = new ArrayList<>();
    private ArrayList<String> first = new ArrayList<>();
    private ArrayList<String> follow = new ArrayList<>();


    public Rule(String t){
        String[] x = t.split("\\s+");
        LHS = x[0];

        for (int i = 2 ; i < x.length ; i ++)
            RHS.add(x[i]);

    }

    public Rule(String LHS, ArrayList<String> RHS) {
        this.LHS = LHS;
        this.RHS = RHS;
    }

    public String getRule(){
        return LHS;
    }

    public ArrayList<String> getRHS() {
        return RHS;
    }


    public ArrayList<String> getFirst() {
        return first;
    }

    public void setFirst(ArrayList<String> first) {
        this.first = first;
    }

    public ArrayList<String> getFollow() {
        return follow;
    }

    public void setFollow(ArrayList<String> follow) {
        this.follow = follow;
    }
}

