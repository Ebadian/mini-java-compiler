package parser;

import errorHandler.ErrorHandler;
import generator.CodeGenerator;
import generator.Memory;
import generator.Quadruples;
import symboltable.SymbolTable;
import scanner.LexicalAnalyzer;
import scanner.Token;
import scanner.TokenType;
import semantic.SemanticAnalyzer;
import semantic.SemanticStack;
import symboltable.exceptions.AlreadyExistsException;

import java.util.ArrayList;
import java.util.Queue;

/**
 * Created by parand on 1/22/18.
 */
public class Parser {
    String file;
    AllRules allRules = new AllRules();
    private ArrayList<String> stack = new ArrayList<>();
    private LexicalAnalyzer scanner;
    private SemanticAnalyzer semanticAnalyzer;
    private CodeGenerator codeGenerator;
    private SymbolTable symbolTable = SymbolTable.getInstance();
    private Quadruples quadruples = new Quadruples();



    public Parser(String file) {
        this.file = file;
        scanner = new LexicalAnalyzer(file);

        SemanticStack ss = new SemanticStack();
        semanticAnalyzer = new SemanticAnalyzer(symbolTable);
        codeGenerator = new CodeGenerator(ss, symbolTable, quadruples);
        Memory.setQuadruples(quadruples);

        ErrorHandler.setScanner(scanner);

        //allRules.print();
    }

    public void parse(){
        Token token ;
        Token lastToken;

        stack.add("EOF");
        stack.add("Source");

        boolean parsing = true;

        lastToken = null;
        token = scanner.getNextToken(lastToken);

        while (parsing){

            if(stack.size() == 0 || token == null)
                break;

            //print();
            String type = token.getLexeme();
            if(token.getTokenType().equals(TokenType.IDENTIFIER))
                type = "identifier";
            else if(token.getTokenType().equals(TokenType.INTEGER))
                type = "integer";

            String top = stack.get(stack.size() - 1);

            //System.out.println(top + " @@@" + token.getLexeme() + "@@@ " + type);
            if(top.charAt(0) =='#'){
                semanticAnalyzer.analyze(top, lastToken);
                codeGenerator.generate(top, lastToken);
                stack.remove(stack.size() - 1);
                continue;
            }

            if(allRules.isTerminal(top)){
                if(type.equals(top)) {
                    stack.remove(stack.size() - 1);
                }

                else
                    ErrorHandler.Error("Parser", token.getLexeme() + " is misplaced.");


                lastToken = token;
                token = scanner.getNextToken(lastToken);
                continue;

            }


            int ruleNum = allRules.getRuleNum(top, type);

            if(ruleNum == -2 ) {
                ErrorHandler.Error("Parser", token.getLexeme() + " is misplaced.");
                lastToken = token;
                token  = scanner.getNextToken(lastToken);
                continue;
            }
            else if(ruleNum == -1) {
                //Sync token
                stack.remove(stack.size() - 1);
                lastToken = token;
                token = scanner.getNextToken(lastToken);
                continue;
            }

            stack.remove(stack.size() - 1);

            for(int j = allRules.rulesForParse.get(ruleNum).getRHS().size() - 1 ; j >= 0 ; j --)
                stack.add(allRules.rulesForParse.get(ruleNum).getRHS().get(j));

        }
        quadruples.print();
    }


    public void print(){
        for (int i = 0 ; i< stack.size(); i ++)
            System.out.print(stack.get(i) + "   ");
        System.out.println();
    }
}




