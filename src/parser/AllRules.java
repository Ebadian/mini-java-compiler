package parser;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Set;

import static utils.ArrayUtils.unique;

/**
 * Created by parand on 1/19/18.
 */
class Entry {
    String nonT;
    String terminal;
    int ruleNum;

    public Entry(String nonT, String terminal, int ruleNum) {
        this.nonT = nonT;
        this.terminal = terminal;
        this.ruleNum = ruleNum;
    }

    public void print() {
        System.out.println(nonT + "   " + terminal + "    " + ruleNum);
    }
}

public class AllRules {

    ArrayList<Entry> ParseTable = new ArrayList<>();
    String[] raw = {
            "Goal -> Source EOF",
            "Source -> ClassDeclarations MainClass",
            "MainClass -> public class Identifier { public static void main ( ) { VarDeclarations Statements } }",
            "ClassDeclarations -> ClassDeclaration ClassDeclarations",
            "ClassDeclarations ->  ",
            "ClassDeclaration -> class Identifier Extension { FieldDeclarations MethodDeclarations }",
            "Extension -> extends Identifier",
            "Extension ->  ",
            "FieldDeclarations -> FieldDeclaration FieldDeclarations",
            "FieldDeclarations ->  ",
            "FieldDeclaration -> static Type Identifier ;",
            "VarDeclarations -> VarDeclaration VarDeclarations",
            "VarDeclarations ->  ",
            "VarDeclaration -> Type Identifier ;",
            "MethodDeclarations -> MethodDeclaration MethodDeclarations",
            "MethodDeclarations ->  ",
            "MethodDeclaration -> public static Type Identifier ( Parameters ) { VarDeclarations Statements return GenExpression ; }",
            "Parameters -> Type Identifier Parameter",
            "Parameters ->  ",
            "Parameter -> , Type Identifier Parameter",
            "Parameter ->  ",
            "Type -> boolean",
            "Type -> int",
            "Statements -> Statement Statements",
            "Statements ->  ",
            "Statement -> { Statements }",
            "Statement -> if ( GenExpression ) Statement else Statement",
            "Statement -> while ( GenExpression ) Statement",
            "Statement -> for ( Identifier = Integer ; Expression Rel2 ; Identifier += Integer ) Statement",
            "Statement -> Identifier = GenExpression ;",
            "Statement -> System.out.println ( GenExpression ) ;",
            "GenExpression -> Expression Gen2",
            "Expression -> Term Expr",
            "Expr ->  ",
            "Expr -> + Term Expr",
            "Expr -> - Term Expr",
            "Term -> Factor T",
            "T -> * Term",
            "T ->  ",
            "Factor -> ( Expression )",
            "Factor -> F1",
            "Factor -> true",
            "Factor -> false",
            "Factor -> Integer",
            "F1 -> Identifier F2",
            "F2 ->  ",
            "F2 -> . Identifier F3",
            "F3 ->  ",
            "F3 -> ( Arguments )",
            "Gen2 -> Rel2 RelExpr2",
            "Gen2 ->  ",
            "RelExpr2 -> && Expression Rel2 RelExpr2",
            "RelExpr2 ->  ",
            "Rel2 -> == Expression",
            "Rel2 -> < Expression",
            "Arguments -> GenExpression Argument",
            "Arguments ->  ",
            "Argument -> , GenExpression Argument",
            "Argument ->  ",
            "Identifier -> identifier",
            "Integer -> integer",

    };

    String[] rawGenerate = {
            "Goal -> Source EOF",
            "Source -> #initial ClassDeclarations MainClass",
            "MainClass -> public class Identifier #decClass { public static void main #decMainMethod ( ) { VarDeclarations Statements } #finishMethod #closeClass } ",
            "ClassDeclarations -> ClassDeclaration ClassDeclarations",
            "ClassDeclarations ->  ",
            "ClassDeclaration -> class Identifier #decClass Extension { FieldDeclarations MethodDeclarations #closeClass }",
            "Extension -> extends Identifier #addExtension",
            "Extension ->  ",
            "FieldDeclarations -> FieldDeclaration FieldDeclarations",
            "FieldDeclarations ->  ",
            "FieldDeclaration -> static Type #saveType Identifier #decField ;",
            "VarDeclarations -> VarDeclaration VarDeclarations",
            "VarDeclarations ->  ",
            "VarDeclaration -> Type #saveType Identifier #addVar ;",
            "MethodDeclarations -> MethodDeclaration MethodDeclarations",
            "MethodDeclarations ->  ",
            "MethodDeclaration -> public static Type #saveType  Identifier #decMethod ( Parameters ) #save #finMethodDec { VarDeclarations Statements return GenExpression #return ; } #jp #finishMethod",
            "Parameters -> Type #saveType Identifier #addParam Parameter",
            "Parameters ->  ",
            "Parameter -> , Type #saveType Identifier #addParam Parameter",
            "Parameter ->  ",
            "Type -> boolean",
            "Type -> int",
            "Statements -> Statement Statements",
            "Statements ->  ",
            "Statement -> { Statements }",
            "Statement -> if ( GenExpression #assignTmp ) #save   Statement else #jpfSave   Statement #jp ",
            "Statement -> while #label ( GenExpression #assignTmp #save ) Statement   #while" ,
            "Statement -> for (  Identifier #pid = Integer #pushInt #assign ; #label Expression Rel2 #assignTmp #save ; Identifier #pid += Integer #pushInt #checkType )  Statement #for",
            "Statement -> Identifier #pid = GenExpression #assign ;",
            "Statement -> System.out.println ( GenExpression #assignTmp ) #print ;",
            "GenExpression -> Expression Gen2 ",
            "Expression -> Term Expr",
            "Expr ->  ",
            "Expr -> + Term #sum Expr",
            "Expr -> - Term #minus Expr",
            "Term -> Factor T",
            "T -> * Term #mult ",
            "T ->  ",
            "Factor -> ( Expression )",
            "Factor -> F1",
            "Factor -> true #pushTrue ",
            "Factor -> false #pushFalse ",
            "Factor -> Integer #pushInt ",
            "F1 -> Identifier  F2 ",
            "F2 ->  #pid ",
            "F2 -> #saveClass . Identifier F3 ",
            "F3 -> #findVar ",
            "F3 -> #addMethodName ( Arguments ) #callMethod ",
            "Gen2 -> Rel2 RelExpr2",
            "Gen2 ->  ",
            "RelExpr2 -> && Expression Rel2 #and RelExpr2",
            "RelExpr2 ->  ",
            "Rel2 -> == Expression #eq ",
            "Rel2 -> < Expression #lt ",
            "Arguments -> GenExpression #addArg Argument ",
            "Arguments ->  ",
            "Argument -> , GenExpression #addArg Argument ",
            "Argument ->  ",
            "Identifier -> identifier",
            "Integer -> integer",

    };


    String[] rawFollows = {
            "Source EOF",
            "MainClass EOF",
            "ClassDeclarations public",
            "ClassDeclaration class public",
            "Extension {",
            "FieldDeclarations public }",
            "FieldDeclaration static public }",
            "VarDeclarations { if while for identifier System.out.println } return",
            "VarDeclaration boolean int { if while for identifier System.out.println } return",
            "MethodDeclarations }",
            "MethodDeclaration public }",
            "Parameters )",
            "Parameter )",
            "Type identifier",
            "Statements } return",
            "Statement { if while for identifier System.out.println } return else",
            "GenExpression ; ) ,",
            "Expression == < ; ) , &&",
            "Expr == < ; ) , &&",
            "Term + - == < ; ) , &&",
            "T + - == < ; ) , &&",
            "Factor * + - == < ; ) , &&",
            "F1 * + - == < ; ) , &&",
            "F2 * + - == < ; ) , &&",
            "F3 * + - == < ; ) , &&",
            "Gen2 ; ) ,",
            "RelExpr2 ; ) ,",
            "Rel2 && ; ) ,",
            "Arguments )",
            "Argument )",
            "Identifier { extends ; ( , ) = += . * + - == < &&",
            "Integer * + - == < ; ) , &&",
    };
    ArrayList<Rule> rules = new ArrayList<>();
    ArrayList<String> AllNames = new ArrayList<>();
    ArrayList<String> terminals = new ArrayList<>();
    ArrayList<Rule> rulesForParse = new ArrayList<>();

    ArrayList<Pair<String, ArrayList<String>>> firsts = new ArrayList<>();
    ArrayList<Pair<String, ArrayList<String>>> follows = new ArrayList<>();

    public AllRules() {
        for (int i = 0; i < raw.length; i++) {
            Rule r = new Rule(raw[i]);
            rules.add(r);
            AllNames.add(r.getRule());

            rules = unique(rules);
            AllNames = unique(AllNames);

            rulesForParse.add(new Rule(rawGenerate[i]));
            rulesForParse = unique(rulesForParse);

        }

        for (int i = 0; i < rules.size(); i++) {
            for (int j = 0; j < rules.get(i).getRHS().size(); j++) {
                String t = rules.get(i).getRHS().get(j);
                terminals.add(t);
            }
        }

        terminals = unique(terminals);
        for (int i = terminals.size() - 1; i >= 0; i--) {
            String s = terminals.get(i);
            for (int j = 0; j < AllNames.size(); j++)
                if (AllNames.get(j).equals(s))
                    terminals.remove(i);
        }

        firstAll();
        followAll();
        makeParseTable();

    }


    public void print() {

        for (int i = 0; i < rulesForParse.size(); i++) {
            System.out.print(rulesForParse.get(i).getRule() + " -> ");
            for (int j = 0; j < rulesForParse.get(i).getRHS().size(); j++)
                System.out.print(rulesForParse.get(i).getRHS().get(j) + " ");
            System.out.println();
        }


        //System.out.println("------------");
//        for (int i = 0; i < follows.size(); i++) {
//
//            System.out.println(follows.get(i).getKey());
//            for (int j = 0; j < follows.get(i).getValue().size(); j++)
//                System.out.print(follows.get(i).getValue().get(j) + "    ");
//            System.out.println("\n@@@@@@@@@");
//            System.out.println();
//        }
////
//        System.out.println();
//        System.out.println("------------");
//        for (int i = 0; i < terminals.size(); i++)
//            System.out.print(terminals.get(i) + "   ");

//        System.out.println("\n------------");


    }


    public boolean isTerminal(String s) {
        for (int i = 0; i < terminals.size(); i++)
            if (terminals.get(i).equals(s))
                return true;
        return false;
    }


    private void firstAll() {
        for (int i = 0; i < AllNames.size(); i++)
            firsts.add(new Pair<>(AllNames.get(i), firstFinder(AllNames.get(i))));
    }

    private ArrayList<String> firstFinder(String name) {
        ArrayList<String> res = new ArrayList<>();

        for (int i = 0; i < rules.size(); i++) {
            Rule r = rules.get(i);
            if (r.getRule().equals(name)) {
                if (r.getRHS().size() == 0) {
                    res.add("eps");
                    continue;
                }

                boolean eps = false;
                boolean cont = true;
                ArrayList<String> rhs = r.getRHS();

                for (int k = 0; k < rhs.size(); k++) {
                    eps = false;
                    if (!cont)
                        break;
                    if (isTerminal(rhs.get(k))) {
                        cont = false;
                        eps = false;
                        res.add(rhs.get(k));
                        break;
                    }
                    ArrayList<String> f = firstFinder(rhs.get(k));

                    for (int x = 0; x < f.size(); x++) {
                        if (f.get(x) == "eps")
                            eps = true;
                        else
                            res.add(f.get(x));
                    }
                    if (!eps)
                        cont = false;

                }
                if (eps)
                    res.add("eps");


            }
            res = unique(res);
        }
        return res;
    }


    public void followAll(){
        for (int i = 0 ; i < rawFollows.length ; i ++){
            String[] s = rawFollows[i].split(" ");
            String rule = s[0];
            ArrayList<String> res = new ArrayList<>();
            for (int j = 1 ; j < s.length; j ++)
                res.add(s[j]);

            //System.out.println("!!! " + rule);
            //for (int j = 0 ; j < res.size() ; j ++)
              //  System.out.print(res.get(j) + " ");
            //System.out.println();
            follows.add(new Pair<>(rule, res));
        }
    }

    public ArrayList<String> getFirst(String name) {
        for (int i = 0; i < firsts.size(); i++)
            if (firsts.get(i).getKey().equals(name))
                return firsts.get(i).getValue();
        return null;
    }

    public ArrayList<String> getFollow(String name) {
        for (int i = 0; i < follows.size(); i++)
            if (follows.get(i).getKey().equals(name))
                return follows.get(i).getValue();
        return null;
    }

    private void makeParseTable(){
        for (int i = 0 ; i < rules.size() ; i ++){
            String r = rules.get(i).getRule();
            ArrayList<String> rhs = rules.get(i).getRHS();
            boolean done = false;
            for (int j = 0 ; j < rhs.size(); j ++){
                if(done)
                    break;
                if(isTerminal(rhs.get(j))){
                    done = true;
                    ParseTable.add(new Entry(r, rhs.get(j), i));
                }
                else{
                    ArrayList<String> f = getFirst(rhs.get(j));
                    if(!f.contains("eps"))
                        done = true;
                    for (int k = 0 ; k < f.size(); k ++)
                        ParseTable.add(new Entry(r,f.get(k), i));

                }
            }
            if(!done){
                ArrayList<String> flw = getFollow(r);
                for (int k = 0 ; k < flw.size(); k ++)
                    ParseTable.add(new Entry(r,flw.get(k), i));
            }

        }
        for (int i = 1 ; i < AllNames.size() ; i ++){
            String r = AllNames.get(i);
            //System.out.println(r + "@@@");
            ArrayList<String> flw = getFollow(r);
            for (int j = 0 ; j < flw.size() ; j ++){
                if(getRuleNum(r, flw.get(j)) == -2)
                    ParseTable.add(new Entry(r, flw.get(j), -1));
            }
        }
    }


    public int getRuleNum(String x, String y){
        for (int i = 0 ; i < ParseTable.size(); i ++)
            if(ParseTable.get(i).nonT.equals(x) && ParseTable.get(i).terminal.equals(y))
                return ParseTable.get(i).ruleNum;
        return -2;
    }
}







