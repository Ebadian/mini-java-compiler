package scanner;

public class Token {
    private TokenType tokenType = null;
    private String lexeme = null;

    public Token() {
    }

    public Token(TokenType tokenType, String lexeme) {
        this.tokenType = tokenType;
        this.lexeme = lexeme;
    }

    public void setTokenType(TokenType tokenType) {
        this.tokenType = tokenType;
    }

    public void setLexeme(String lexeme) {
        this.lexeme = lexeme;
    }

    public void appendLexeme(char c) {
        if (this.lexeme == null)
            this.lexeme = "";
        this.lexeme += c;
    }

    public String getLexeme() {
        return lexeme;
    }

    public TokenType getTokenType() {
        return tokenType;
    }
}
