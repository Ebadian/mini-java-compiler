package scanner;

public enum TokenType {
    EOF("EOF"),
    PUBLIC("public"),
    CLASS("class"),
    LBR("{"),
    RBR("}"),
    STATIC("static"),
    VOID("void"),
    MAIN("main"),
    LPR("("),
    RPR(")"),
    EXTENDS("extends"),
    RETURN("return"),
    SEMICOLON(";"),
    COMMA(","),
    BOOLEAN("boolean"),
    INT("int"),
    IF("if"),
    ELSE("else"),
    WHILE("while"),
    FOR("for"),
    PRINT("System.out.println"),
    PLUS("+"),
    MINUS("-"),
    MUL("*"),
    COLON("."),
    TRUE("true"),
    FALSE("false"),
    ASSIGN("="),
    SUMASSIGN("+="),
    AND("&&"),
    EQUAL("=="),
    LT("<"),
    IDENTIFIER(null),
    INTEGER(null),
    ERROR(null);

    String regexString;

    TokenType(String regexString) {
        this.regexString = regexString;
    }
}
