package scanner;

import errorHandler.ErrorHandler;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class LexicalAnalyzer {
    private Scanner scanner;

    public LexicalAnalyzer(String fileName) {
        try {
            File file = new File(fileName);
            this.scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.err.println("File " + fileName + " not found");
        }
        lineCounter = 0;
    }

    private int lineCounter = 0;

    public int getLineCounter() {
        return lineCounter;
    }

    private String lineBuffer = null;
    private int linePointer = 0;

    public int getLinePointer() {
        return linePointer;
    }

    private Character getNextChar() {
        if (endOfLine()) {
            if (!scanner.hasNextLine()) {
                System.err.println("End of File has reached, returning Null.");
                return null;
            }
            linePointer = 0;
            lineCounter++;
            lineBuffer = scanner.nextLine() + "\n";
        }
        Character c = lineBuffer.charAt(linePointer++);
        return c;
    }

    enum TState {
        START,
        DONE,
        NUM,
        PLUS,
        MINUS,
        AND,
        EQ,
        ID,
        COMMENT,
        ONE_LINE_COMMENT,
        MULTI_LINE_COMMENT,
        FIN_COMMENT,
        PRINT
    }

    public Token getNextToken(Token lastToken) {
        Token token = new Token();
        TState state = TState.START;

        Integer outPrintlnIndex = null;

        while (state != TState.DONE) {
            Character c = getNextChar();
            if (c == null) {
                token.setTokenType(TokenType.ERROR);
                break;
            }

            boolean saveCharacter = true;
            boolean unreadLastCharacter = false;
            switch (state) {
                case START:
                    if (Character.isWhitespace(c)) {
                        saveCharacter = false;
                    } else if (Character.isDigit(c)) {
                        state = TState.NUM;
                    } else if (Character.isLetter(c)) {
                        state = TState.ID;
                    } else if (c == '+') {
                        state = TState.PLUS;
                    } else if (c == '-') {
                        state = TState.MINUS;
                    } else if (c == '&') {
                        state = TState.AND;
                    } else if (c == '=') {
                        state = TState.EQ;
                    } else if (c == '/') {
                        saveCharacter = false;
                        state = TState.COMMENT;
                    } else {
                        boolean nextStateIsDone = true;
                        switch (c) {
                            case '{':
                                token.setTokenType(TokenType.LBR);
                                break;
                            case '}':
                                token.setTokenType(TokenType.RBR);
                                break;
                            case '(':
                                token.setTokenType(TokenType.LPR);
                                break;
                            case ')':
                                token.setTokenType(TokenType.RPR);
                                break;
                            case ';':
                                token.setTokenType(TokenType.SEMICOLON);
                                break;
                            case '.':
                                token.setTokenType(TokenType.COLON);
                                break;
                            case ',':
                                token.setTokenType(TokenType.COMMA);
                                break;
                            case '*':
                                token.setTokenType(TokenType.MUL);
                                break;
                            case '<':
                                token.setTokenType(TokenType.LT);
                                break;
                            default:
                                nextStateIsDone = false;
                                ErrorHandler.Error("Lexical Analyzer", "Not recognized character \"" + c
                                        + "\". Skipping Character.");
                                saveCharacter = false;
                                break;
                        }
                        if (nextStateIsDone)
                            state = TState.DONE;
                    }
                    break;
                case NUM:
                    if (!Character.isDigit(c)) {
                        state = TState.DONE;
                        token.setTokenType(TokenType.INTEGER);
                        unreadLastCharacter = true;
                    }
                    break;
                case PLUS:
                    if (c == '=') {
                        state = TState.DONE;
                        token.setTokenType(TokenType.SUMASSIGN);
                    } else if (Character.isDigit(c)) {
                        if (lastToken != null &&
                                (lastToken.getTokenType() == TokenType.IDENTIFIER
                                        || lastToken.getTokenType() == TokenType.INTEGER)) {
                            state = TState.DONE;
                            token.setTokenType(TokenType.PLUS);
                            unreadLastCharacter = true;
                        } else {
                            state = TState.NUM;
                        }
                    } else {
                        state = TState.DONE;
                        token.setTokenType(TokenType.PLUS);
                        unreadLastCharacter = true;
                    }
                    break;
                case MINUS:
                    if (Character.isDigit(c)) {
                        state = TState.NUM;
                    } else {
                        state = TState.DONE;
                        token.setTokenType(TokenType.MINUS);
                        unreadLastCharacter = true;
                    }
                    break;
                case AND:
                    if (c == '&') {
                        state = TState.DONE;
                        token.setTokenType(TokenType.AND);
                    } else {
                        ErrorHandler.Error("Lexical Analyzer", "Expected \"&\" but found \"" + c
                                + "\". Skipping Character.");
                        saveCharacter = false;
                    }
                    break;
                case EQ:
                    state = TState.DONE;
                    if (c == '=') {
                        token.setTokenType(TokenType.EQUAL);
                    } else {
                        token.setTokenType(TokenType.ASSIGN);
                        unreadLastCharacter = true;
                    }
                    break;
                case ID:
                    if (c == '.' && token.getLexeme().equals("System")) {
                        outPrintlnIndex = 0;
                        state = TState.PRINT;
                    } else if (c == '.' || !Character.isLetterOrDigit(c)) {
                        state = TState.DONE;
                        token.setTokenType(TokenType.IDENTIFIER);
                        unreadLastCharacter = true;
                    }
                    break;
                case PRINT:
                    String expected = "out.println";
                    if (outPrintlnIndex == null) {
                        throw new RuntimeException("out.println index should not be null");
                    }

                    if (outPrintlnIndex == expected.length()) {
                        unreadLastCharacter = true;
                        state = TState.DONE;
                        token.setTokenType(TokenType.PRINT);
                    } else if (c == expected.charAt(outPrintlnIndex)) {
                        outPrintlnIndex += 1;
                    } else {
                        ErrorHandler.Error("Lexical Analyzer", "Expected \"" + expected.charAt(outPrintlnIndex)
                                + "\" but found \"" + c + "\". Skipping Character.");
                    }
                    break;
                case COMMENT:
                    saveCharacter = false;
                    if (c == '/') {
                        state = TState.ONE_LINE_COMMENT;
                    } else if (c == '*') {
                        state = TState.MULTI_LINE_COMMENT;
                    } else {
                        ErrorHandler.Error("Lexical Analyzer", "Comment started, Expected \"/\" or \"*\" but found \""
                                + c + "\". Skipping Character.");
                    }
                    break;
                case ONE_LINE_COMMENT:
                    saveCharacter = false;
                    if (c == '\n')
                        state = TState.START;
                    break;
                case MULTI_LINE_COMMENT:
                    saveCharacter = false;
                    if (c == '*') {
                        state = TState.FIN_COMMENT;
                    }
                    break;
                case FIN_COMMENT:
                    saveCharacter = false;
                    if (c == '/') {
                        state = TState.START;
                    } else {
                        state = TState.MULTI_LINE_COMMENT;
                    }
                    break;
                default:
                    break;
            }
            if (unreadLastCharacter) {
                saveCharacter = false;
                unreadChar();
            }
            if (saveCharacter)
                token.appendLexeme(c);
        }

        if (token.getLexeme() == null)
            return null;

        if (token.getTokenType() == TokenType.IDENTIFIER) {
            for (TokenType tokenType: TokenType.values()) {
                if (token.getLexeme().equals(tokenType.regexString)) {
                    token.setTokenType(tokenType);
                    break;
                }
            }
        }
        return token;
    }

    private void unreadChar() {
        linePointer--;
        if (linePointer < 0) {
            throw new RuntimeException("LinePointer should never get below 0!");
        }
    }

    private boolean endOfLine() {
        return lineBuffer == null || lineBuffer.length() <= linePointer;
    }

    public boolean endOfFile() {
        return !scanner.hasNextLine() && endOfLine();
    }
}
